# FluxCD Config

## Create a personal access token

To authenticate with the Flux CLI, create a **personal access token** with the **api** scope:

* On the left sidebar, select your avatar.
* Select **Edit profile**.
* On the left sidebar, select **Access Tokens**.
* Enter a name and optional expiry date for the token.
* Select the **api** scope.
* Select **Create personal access token**.

## Flux gitlab bootstrap

For accessing the GitLab API, the boostrap command requires a GitLab personal access token (PAT) with complete read/write access to the GitLab API.

The GitLab PAT can be exported as an environment variable:
`export GITLAB_TOKEN=<glpat-token>`

Run the bootstrap for a project:

```bash
flux bootstrap gitlab \
  --components-extra=image-reflector-controller,image-automation-controller \ # if you plan to use ImageAutomation
  --token-auth=false \
  --owner=<gitlab_account_owner> \
  --repository=flux-config \
  --branch=main \
  --path=clusters/workshop \
  --personal \
  --read-write-key=true
```